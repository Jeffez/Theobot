package Utils;

public class Variable {

	private static String token = "MjMyMTI1NDY1NjgxMzMwMTc2.CtVJZw.bRkZlAQv5BZW_hPnjxZmJh5V7QM";
	//private static String generalId = "232937360646602762";
	private static String generalId = "231870365863903232";
	private static String admin = "106474775546712064";
	private static String daClientId = "5069";
	private static String daClientSecret = "37460b26a89f8512d7fd08bc95819aeb";
	private static String keyRule = "Dsa5f2cdSSxc58e4DDScx2s5a8x3zs9d";
	private static String theodoreId = "232158657004044289";
	private static String guildId = "231870365863903232";

	////////////////////////////////////////////

	public static String getToken() {
		return token;
	}

	////////////////////////////////////////////

	public static String getGeneral() {
		return generalId;
	}

	////////////////////////////////////////////

	public static String getAdmin() {
		return admin;
	}

	////////////////////////////////////////////

	public static String getDeviantArtClientId() {
		return daClientId;
	}

	////////////////////////////////////////////

	public static String getDeviantArtClientSecret() {
		return daClientSecret;
	}

	////////////////////////////////////////////

	public static String getKeyRule() {
		return keyRule;
	}

	////////////////////////////////////////////

	public static String getTheodoreId() {
		return theodoreId;
	}

	////////////////////////////////////////////

	public static String getGuildId() {
		return guildId;
	}
}
