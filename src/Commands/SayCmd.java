package Commands;

import Utils.Variable;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class SayCmd implements Command {

	private final String NAME = "say";
	private final String HELP = "Usage !" + NAME;
	private String messageKo = "Vous ne possedez pas les droits n�cessairees � l'execution de cette commande";

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		if (event.getAuthor().getId().equals(Variable.getAdmin())) {
			String message = "";
			String id = args[0];
			args[0] = "";
			message = event.getMessage().getContent().replaceAll("!say "+id, "");
			main.jda.getTextChannelById(id).sendMessage(message).queue();
		} else {
			event.getTextChannel().sendMessage(messageKo).queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
