package Commands;

import java.io.*;
import java.net.*;
import java.util.*;

import com.google.gson.*;

import Utils.*;
import net.dv8tion.jda.core.events.message.*;
import thebot.*;

public class DeviantartCmd implements Command {

	private final String NAME = "deviantart";
	private final String HELP = "Usage !" + NAME + " #search";
	private final String URLDATOKEN_1 = "https://www.deviantart.com/oauth2/token?client_id=";
	private final String URLDATOKEN_2 = "&client_secret=";
	private final String URLDATOKEN_3 = "&grant_type=client_credentials";
	private final String URLDAAPI_1 = "https://www.deviantart.com/api/v1/oauth2/browse/tags?tag=";
	private final String URLDAAPI_2 = "&access_token=";

	private String token;
	private ArrayList<String> links;
	private String tag;
	private File file;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		links = new ArrayList<String>();
		if (args.length > 0) {
			tag = args[0];
			try {
				getToken();
				fillList();
				dlRandomFile();
				event.getTextChannel().sendFile(file, event.getMessage()).queue();
			} catch (Exception e) {
				e.printStackTrace();
				event.getTextChannel().sendMessage("J'ai pas reussi :sob: \r\n"
						+ "Tu peux retenter, mais je ne te garantit pas de reussir :sweat_smile:").queue();
			}
		} else {
			event.getTextChannel()
					.sendMessage(
							":thinking: Je pense que tu devrais m'indiquer ce que je dois rechercher sur DeviantArt")
					.queue();
		}
		links.clear();
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

	////////////////////////////////////////////

	public void getToken() {
		System.out.println("GET TOKEN");
		token = "";
		String clientId = Variable.getDeviantArtClientId();
		String clientSecret = Variable.getDeviantArtClientSecret();

		String sURL = URLDATOKEN_1 + clientId + URLDATOKEN_2 + clientSecret + URLDATOKEN_3;
		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();

			request.connect();

			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			JsonObject rootobj = root.getAsJsonObject();

			token = rootobj.get("access_token").getAsString();

			System.out.println(token);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	////////////////////////////////////////////

	public void fillList() {
		System.out.println("FILL LIST");
		String sURL = URLDAAPI_1 + tag + URLDAAPI_2 + token;

		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();

			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			JsonObject rootobj = root.getAsJsonObject();
			JsonArray rootArray = rootobj.getAsJsonArray("results");

			int i = 0;
			for (JsonElement subobj : rootArray) {
				i++;
				if (i >= 100)
					break;
				try {
					String link = subobj.getAsJsonObject().get("content").getAsJsonObject().get("src").getAsString();
					System.out.println(link);
					links.add(link);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	////////////////////////////////////////////

	public void dlRandomFile() {
		Random rand = new Random();
		int answer = rand.nextInt(links.size());
		String fileUrl = links.get(answer);

		String fileName = "";

		InputStream input = null;
		FileOutputStream writeFile = null;

		try {

			URL url = new URL(fileUrl);
			URLConnection connection = url.openConnection();

			int fileLength = connection.getContentLength();

			if (fileLength == -1) {
				return;
			}

			try {

				int read;
				input = connection.getInputStream();
				fileName = url.getFile().substring(url.getFile().lastIndexOf(47) + 1);
				writeFile = new FileOutputStream("images/" + fileName);
				byte[] buffer = new byte[1024];

				while ((read = input.read(buffer)) > 0) {
					writeFile.write(buffer, 0, read);
				}

				writeFile.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {

				writeFile.close();
				input.close();

			} catch (IOException e) {

				e.printStackTrace();

			}
		}

		file = new File("images/" + fileName);
	}
}
