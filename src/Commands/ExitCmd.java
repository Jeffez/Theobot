package Commands;

import Utils.Variable;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class ExitCmd implements Command {

	private final String NAME = "exit";
	private final String HELP = "Usage !" + NAME;
	private String messageOk = "Deconnexion imminente...";
	private String messageKo = "Vous ne possedez pas les droits n�cessairees � l'execution de cette commande";

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		if (event.getAuthor().getId().equals(Variable.getAdmin())) {
			event.getTextChannel().sendMessage(messageOk).queue();
			main.jda.shutdown();
		} else {
			event.getTextChannel().sendMessage(messageKo).queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
