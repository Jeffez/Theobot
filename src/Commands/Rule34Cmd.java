package Commands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Random;

import javax.net.ssl.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class Rule34Cmd implements Command {

	private final String NAME = "rule34";
	private final String HELP = "Usage !" + NAME + " #search";
	private final String URLRAPI = "http://jetruccharasch.fr/site/theobot/json.php?ruleid=";

	private ArrayList<String> links;
	private String tag;
	private File file;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		if (!event.getTextChannel().getId().equals("258685433800687616")
				|| !event.getTextChannel().getId().equals("231886536608710657")) {
			event.getTextChannel().sendMessage(":cold_sweat: Hum... Tu ne te serais pas tromp� de channel ?? Go sur "
					+ main.jda.getTextChannelById("258685433800687616").getAsMention()).queue();
		}
		links = new ArrayList<String>();
		if (args.length > 0) {
			tag = args[0];
			try {
				fillList();
				dlRandomFile();
				event.getTextChannel().sendFile(file, event.getMessage()).queue();
			} catch (Exception e) {
				e.printStackTrace();
				event.getTextChannel().sendMessage("J'ai pas reussi :sob: \r\n"
						+ "Tu peux retenter, mais je ne te garantit pas de reussir :sweat_smile:").queue();
			}
		} else {
			event.getTextChannel()
					.sendMessage(
							":thinking: Je pense que tu devrais m'indiquer ce que je dois rechercher sur DeviantArt")
					.queue();
		}
		links.clear();

	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

	////////////////////////////////////////////

	public void fillList() {
		System.out.println("FILL LIST");
		String sURL = URLRAPI + tag;

		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();

			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			JsonObject rootobj = root.getAsJsonObject();
			JsonArray rootArray = rootobj.getAsJsonArray("posts").get(0).getAsJsonObject().getAsJsonArray("post");

			int i = 0;
			for (JsonElement subobj : rootArray) {
				i++;
				if (i >= 100)
					break;
				try {
					String link = subobj.getAsJsonObject().get("$").getAsJsonObject().get("file_url").getAsString();
					System.out.println(link);
					links.add(link);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	////////////////////////////////////////////

	public void dlRandomFile() {
		Random rand = new Random();
		int answer = rand.nextInt(links.size());
		String fileUrl = links.get(answer);

		String fileName = "";

		InputStream input = null;
		FileOutputStream writeFile = null;

		try {

			URL url = new URL(fileUrl);
			URLConnection connection = url.openConnection();

			int fileLength = connection.getContentLength();

			if (fileLength == -1) {
				return;
			}
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
				}
			} };

			// Activate the new trust manager
			try {
				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			} catch (Exception e) {
			}
			try {

				int read;
				input = connection.getInputStream();
				fileName = url.getFile().substring(url.getFile().lastIndexOf(47) + 1);
				writeFile = new FileOutputStream("images/" + fileName);
				byte[] buffer = new byte[1024];

				while ((read = input.read(buffer)) > 0) {
					writeFile.write(buffer, 0, read);
				}

				writeFile.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				writeFile.close();
				input.close();

			} catch (IOException e) {

				e.printStackTrace();

			}
		}

		file = new File("images/" + fileName);
	}
}
