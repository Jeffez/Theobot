package Commands;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class BetCmd implements Command {

	private final String NAME = "bet";
	private final String HELP = "Usage !" + NAME;
	
	
	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		if(args.length>0) {
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/y");
			try {
				Date date = formatter.parse(args[0]);
				main.bets.put(event.getAuthor().getId(), date);
				event.getTextChannel().sendMessage("J'ai bien enregistr� ton pari :wink:").queue();
			}catch(Exception e) {
				event.getTextChannel().sendMessage("J'ai pas trouv� la commande :sob: \r\n"
						+ "Tu es sur que tu ne t'es pas tromp� quelque part ? :thinking:").queue();
				e.printStackTrace();
			}
		}else {
			event.getTextChannel().sendMessage("J'ai pas trouv� la commande :sob: \r\n"
					+ "Tu es sur que tu ne t'es pas tromp� quelque part ? :thinking:").queue();
		
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
