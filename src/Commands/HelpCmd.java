package Commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class HelpCmd implements Command {

	private final String NAME = "help";
	private final String HELP = "Usage !" + NAME;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		try {
			event.getTextChannel().sendMessage("Help :\r\n" + "" + main.commands.get(args[0]).help()).queue();
		} catch (Exception e) {
			e.printStackTrace();
			event.getTextChannel().sendMessage("J'ai pas trouv� la commande :sob: \r\n"
					+ "Tu es sur que tu ne t'es pas tromp� quelque part ? :thinking:").queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
