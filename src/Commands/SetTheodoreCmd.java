package Commands;

import java.time.OffsetDateTime;

import Utils.Variable;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class SetTheodoreCmd implements Command {

	private final String NAME = "setTheodore";
	private final String HELP = "Usage !" + NAME;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		event.getTextChannel().sendMessage(OffsetDateTime.now().toEpochSecond()+"").queue();
		if (event.getAuthor().getId().equals(Variable.getAdmin())) {
			main.theodoreConnection = Long.parseLong(args[0]);
			event.getTextChannel().sendMessage("Done :D").queue();
			
		} else {
			event.getTextChannel().sendMessage("T'as pas le droit :p").queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
