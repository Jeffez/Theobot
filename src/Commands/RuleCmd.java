package Commands;

import java.io.*;
import java.net.*;

import com.google.gson.*;

import net.dv8tion.jda.core.events.message.*;
import thebot.*;

public class RuleCmd implements Command {

	private final String NAME = "rule";
	private final String HELP = "Usage !" + NAME;

	private String message;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		message = "";
		if (args.length > 0) {
			System.out.println(args[0]);
			if (args[0].equals("all")) {
				event.getTextChannel().sendMessage(
						":scales: L'ensemble des r�gles se situe sur :  http://theobot.jetruccharasch.fr/rules.php \r\n"
								+ "Ou sur " + main.jda.getTextChannelById("263052530693439488").getAsMention())
						.queue();
			} else {
				getRule(args[0]);
				event.getTextChannel().sendMessage(message).queue();
			}
		} else {
			event.getTextChannel()
					.sendMessage(
							":thinking: Je pense que tu devrais m'indiquer la regle que je dois chercher")
					.queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

	////////////////////////////////////////////

	public void getRule(String id) {
		String sURL = "http://jetruccharasch.fr/site/theobot/json.php?id=" + id;
		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			JsonObject rootobj = root.getAsJsonObject();
			String creator = rootobj.get("creator").getAsString();
			String rule = rootobj.get("rule").getAsString();
			message = ":scales: R�gle " + id + " :\r\n" + rule + "\r\n" + "Inscrite par : " + creator;
		} catch (IOException e) {
			e.printStackTrace();
			message = "J'ai pas compris :sob: \r\n"
					+ "Tu peux retenter ta chance, ou abandonner :stuck_out_tongue_winking_eye: ";
		} catch (IllegalStateException e) {
			message = "D�sol�, je ne connais pas cette r�gle :confused: ";
		}
	}
}
