package Commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.jsoup.Jsoup;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class DtcCmd implements Command {

	private final String NAME = "dtc";
	private final String HELP = "Usage !" + NAME;
	private final String URLAPI = "http://jetruccharasch.fr/dtc/api/quote/?quoteId=";
	private final String URLAPIRAND = "http://jetruccharasch.fr/dtc/api/quote/random/";
	
	private String message;
	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		message = "";
		if (args.length > 0) {
			if(args[0].equals("rand")) {
				getRandQuote();
			}else {
				getQuote(args[0]);
			}
			event.getTextChannel().sendMessage(message).queue();
			
		} else {
			event.getTextChannel()
					.sendMessage(
							":thinking: Je pense que tu devrais m'indiquer ce que je dois rechercher sur DeviantArt")
					.queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

	////////////////////////////////////////////

	public void getQuote(String id) {
		String sURL = URLAPI + id;
		message = "Quote "+id +" : \r\n"
				+ "```Markdown\r\n";
		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();
			System.out.println("Connected");
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			JsonObject rootobj = root.getAsJsonObject();
			JsonArray quote = rootobj.get("quote").getAsJsonArray();
			for(JsonElement je : quote) {
				message+= "<" + Jsoup.parse(je.getAsJsonObject().get("user").getAsString()).text() + ">";
				message+= Jsoup.parse(je.getAsJsonObject().get("sentence").getAsString()).text() + "\r\n";
			}
			message+="```\r\n"
					+ "(+)" + rootobj.get("plus").getAsString() + " (-)" + rootobj.get("minus").getAsString();
			message+="\r\n"
					+ "https://danstonchat.com/"+id+".html";
		} catch (IOException e) {
			e.printStackTrace();
			message = "J'ai pas compris :sob: \r\n"
					+ "Tu peux retenter ta chance, ou abandonner :stuck_out_tongue_winking_eye: ";
		} catch (IllegalStateException e) {
			e.printStackTrace();
			message = "D�sol�, je ne connais pas cette quote :confused: ";
		}
	}

	////////////////////////////////////////////

	public void getRandQuote() {
		String sURL = URLAPIRAND;
		
		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();
			System.out.println("Connected");
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			JsonObject rootobj = root.getAsJsonObject();
			JsonArray quote = rootobj.get("quote").getAsJsonArray();
			message = "Quote "+ rootobj.get("quoteId").getAsString() +" : \r\n"
					+ "```Markdown\r\n";
			for(JsonElement je : quote) {
				message+= "<" + Jsoup.parse(je.getAsJsonObject().get("user").getAsString()).text() + ">";
				message+= Jsoup.parse(je.getAsJsonObject().get("sentence").getAsString()).text() + "\r\n";
			}
			message+="```\r\n"
					+ "(+)" + rootobj.get("plus").getAsString() + " (-)" + rootobj.get("minus").getAsString();
			message+="\r\n"
					+ "https://danstonchat.com/"+rootobj.get("quoteId").getAsString()+".html";
		} catch (IOException e) {
			e.printStackTrace();
			message = "J'ai pas compris :sob: \r\n"
					+ "Tu peux retenter ta chance, ou abandonner :stuck_out_tongue_winking_eye: ";
		} catch (IllegalStateException e) {
			e.printStackTrace();
			message = "D�sol�, je ne connais pas cette quote :confused: ";
		}
	}
}
