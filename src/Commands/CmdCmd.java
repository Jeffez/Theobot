package Commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class CmdCmd implements Command {

	private final String NAME = "cmd";
	private final String HELP = "Usage !" + NAME;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		String message ="La liste des commandes disponible est : \r\n";
		for(String key : main.commands.keySet()) {
			message += "!" +key+"\r\n";
		}
		event.getTextChannel().sendMessage(message).queue();
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
