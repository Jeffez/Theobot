package Commands;

import java.time.OffsetDateTime;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class PingCmd implements Command {

	private final String NAME = "ping";
	private final String HELP = "Usage !" + NAME;
	private long messageTime;
	private long actualTime;
	private String quoteMessage = "[jeudi 27 ao�t 2015 21:41] Th�odore: avoir du ping pour les idiot";
	private String pingMessage = "Mon ping est :";

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		event.getTextChannel().sendMessage(quoteMessage).queue();
		messageTime = event.getMessage().getCreationTime().toEpochSecond();
		actualTime = OffsetDateTime.now().toEpochSecond();
		event.getTextChannel().sendMessage(pingMessage + (actualTime - messageTime)).queue();
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
