package Commands;

import java.util.Random;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class RandCmd implements Command {

	private final String NAME = "rand";
	private final String HELP = "Usage !" + NAME;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		if (args.length > 0) {
			event.getTextChannel().sendMessage(":game_die: " + event.getAuthor().getName() + " -> " + rand(args[0]))
					.queue();
		} else {
			event.getTextChannel().sendMessage(":game_die: " + event.getAuthor().getName() + " -> " + rand("")).queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);

	}

	////////////////////////////////////////////

	public int rand(String arg) {
		int max;
		if (arg.equals("")) {
			max = 100;
		} else {
			max = Integer.parseInt(arg);
		}

		Random rand = new Random();
		int answer = rand.nextInt(max);
		return answer;
	}

}
