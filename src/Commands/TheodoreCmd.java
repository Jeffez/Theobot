package Commands;

import java.time.OffsetDateTime;

import Utils.Variable;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.*;

public class TheodoreCmd implements Command {

	private final String NAME = "theodore";
	private final String HELP = "Usage !" + NAME;

	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(main.theodoreConnection);
		if (event.getGuild().getMember(main.jda.getUserById(Variable.getTheodoreId())).getOnlineStatus()
				.equals(OnlineStatus.ONLINE)) {
			event.getTextChannel().sendMessage(":thinking: Comment te dire qu'il est actuellement connect�").queue();
		} else if (main.theodoreConnection == 0) {
			event.getTextChannel().sendMessage("Mais qui est ce Th�odore dont vous parlez tous ?\r\n"
					+ "Je ne l'ai jamais vu pour le moment :sob:").queue();
		} else {
			long actualTime = OffsetDateTime.now().toEpochSecond();
			String time = "";
			long timeLong = actualTime - main.theodoreConnection;
			time = (timeLong % 60) + " secondes" + time;
			time = ((timeLong / 60) % 60) + " minutes et " + time;
			time = ((timeLong / 3600) % 24) + " heures, " + time;
			time = (timeLong / 86400) + " jours, " + time;
			event.getGuild().getTextChannelById(Variable.getGeneral())
					.sendMessage("La derni�re fois que j'ai vu Theodore date d'il y a :\r\n" + time).queue();

		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

}
