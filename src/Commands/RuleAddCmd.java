package Commands;

import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import Utils.Variable;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import thebot.Command;

public class RuleAddCmd implements Command {

	private final String NAME = "ruleadd";
	private final String HELP = "Usage !" + NAME;

	private String rule = "";
	private boolean ruleAlreadyExist = false;
	private String message;
	////////////////////////////////////////////

	@Override
	public boolean called(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	////////////////////////////////////////////

	@Override
	public void action(String[] args, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		message = "";
		if (args.length > 1) {
			getRule(args[0]);
			rule = event.getMessage().getContent().replaceAll("!ruleadd " + args[0], "");
			if (!ruleAlreadyExist) {
				addRule(args, event.getAuthor().getName());
				event.getTextChannel().sendMessage(message).queue();
			} else {
				event.getTextChannel().sendMessage("Cette r�gle existe d�j� :sob:").queue();
			}
		} else {
			event.getTextChannel().sendMessage("J'ai pas compris :sob: \r\n"
					+ "            		+ \"Tu peux retenter ta chance, ou abandonner :stuck_out_tongue_winking_eye: ")
					.queue();
		}
	}

	////////////////////////////////////////////

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return HELP;
	}

	////////////////////////////////////////////

	@Override
	public void executed(boolean sucess, MessageReceivedEvent event) {
		// TODO Auto-generated method stub
		System.out.println(String.valueOf(event.getAuthor().getName()) + " use the command !" + NAME);
	}

	////////////////////////////////////////////

	public void getRule(String id) {
		String sURL = "http://jetruccharasch.fr/site/theobot/json.php?id=" + id;
		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.connect();
			ruleAlreadyExist = false;
		} catch (IOException e) {
			e.printStackTrace();
			ruleAlreadyExist = true;
		}
	}

	////////////////////////////////////////////

	private void addRule(String[] args, String creator) {
		// TODO Auto-generated method stub
		String id = args[0];

		String key = Variable.getKeyRule();
		String sURL = "http://jetruccharasch.fr/site/theobot/add.php";

		try {
			URL url = new URL(sURL);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.setDoOutput(true);
			PrintStream ps = new PrintStream(request.getOutputStream());
			ps.print("creator=" + creator);
			ps.print("&rule=" + rule);
			ps.print("&id=" + id);
			ps.print("&key=" + key);

			request.getInputStream();
			ps.close();
			message = "J'ai reussi \\o/";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "J'ai pas reussi :sob:\r\n" + "Retente ta chance :wink:";
		}

	}

}
