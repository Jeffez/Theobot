package thebot;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import Commands.*;
import Utils.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.audio.AudioSendHandler;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.managers.AudioManager;

public class main {

	public static JDA jda;
	public static final CommandParser parser;
	public static HashMap<String, Command> commands;
	public static long theodoreConnection;
	public static HashMap<String,Date> bets;

	////////////////////////////////////////////

	static {
		parser = new CommandParser();
		commands = new HashMap<String, Command>();
	}

	////////////////////////////////////////////

	public static void main(String[] args) {
		bets = new HashMap<String,Date>();
		// TODO Auto-generated method stub
		try {
			jda = new JDABuilder(AccountType.BOT).setToken(Variable.getToken()).buildAsync();
			jda.addEventListener(new Listenner());
			jda.setAutoReconnect(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		addCommand();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		Guild coucou = jda.getGuildById("231870365863903232");
		coucou.getAudioManager().openAudioConnection(jda.getVoiceChannelById("231883587836706816"));
		Player player = new FilePlayer(new File("Tuturu.wav"));
		AudioSendHandler ash;
        coucou.getAudioManager().setSendingHandler(ash);
		//jda.getTextChannelById(Variable.getGeneral()).sendMessage("Re !").queue();
*/
	}

	////////////////////////////////////////////

	public static void addCommand() {
		commands.put("ping", new PingCmd());
		commands.put("logs", new LogsCmd());
		commands.put("who", new WhoCmd());;
		commands.put("dtc", new DtcCmd());
		commands.put("help", new HelpCmd());
		commands.put("exit", new ExitCmd());
		commands.put("rand", new RandCmd());
		commands.put("say", new SayCmd());
		commands.put("rule34", new Rule34Cmd());
		commands.put("deviant", new DeviantartCmd());
		commands.put("rule", new RuleCmd());
		commands.put("ruleadd", new RuleAddCmd());
		commands.put("theodore", new TheodoreCmd());
		commands.put("haruhi", new HaruhiCmd());
		commands.put("cmd", new CmdCmd());
		commands.put("bet", new BetCmd());
		commands.put("settheodore", new SetTheodoreCmd());
		
		
		
	}

	////////////////////////////////////////////

	public static void handleCommand(CommandParser.CommandContainer cmd) {
		if (commands.containsKey(cmd.invoke)) {
			boolean safe = commands.get(cmd.invoke).called(cmd.args, cmd.event);
			if (safe) {
				commands.get(cmd.invoke).action(cmd.args, cmd.event);
				commands.get(cmd.invoke).executed(safe, cmd.event);
			} else {
				commands.get(cmd.invoke).executed(safe, cmd.event);
			}
		}
	}
}
