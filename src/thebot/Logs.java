package thebot;

import java.io.*;
import java.net.*;
import net.dv8tion.jda.core.events.message.*;

public class Logs {

	private static String sURL = "http://jetruccharasch.fr/site/theobot/addMessage.php";

	////////////////////////////////////////////

	public static void insert(MessageReceivedEvent event) {
		try {
			URL url = new URL(sURL);
			URLConnection con = url.openConnection();
			con.setDoOutput(true);
			PrintStream ps = new PrintStream(con.getOutputStream());
			ps.print("conv=" + event.getGuild().getId());
			ps.print("&author=" + event.getAuthor().getId());
			ps.print("&message=" + event.getMessage().getContent());
			con.getInputStream();
			ps.close();
		} catch (Exception url) {
			// empty catch block
		}
	}
}
