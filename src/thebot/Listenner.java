package thebot;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map.Entry;

import Utils.*;
import net.dv8tion.jda.client.managers.EmoteManager;
import net.dv8tion.jda.client.managers.fields.EmoteField;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.user.*;
import net.dv8tion.jda.core.hooks.*;

public class Listenner extends ListenerAdapter {

	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		
		if (event.getClass() == MessageReceivedEvent.class) {
			Logs.insert(event);
			System.out.println(event.getAuthor().getName() + ":" + event.getMessage().getContent() + "\n");
			if (event.getMessage().getContent().startsWith("!") && !event.getAuthor().isBot()) {

				main.handleCommand(main.parser.parse(event.getMessage().getContent().toLowerCase(), event));

			}
		}
	}

	public void onMessageReactionAdd(MessageReactionAddEvent event) {
        System.out.println(event.getReaction());
	}
	
	////////////////////////////////////////////

	@Override
	public void onUserOnlineStatusUpdate(UserOnlineStatusUpdateEvent event) {
		if (event.getUser().equals(main.jda.getUserById(Variable.getTheodoreId()))
				&& event.getGuild().getId().equals(Variable.getGuildId())) {
			if (event.getGuild().getMember(event.getUser()).getOnlineStatus().equals(OnlineStatus.ONLINE)) {
				
				Date today = Date.from(Instant.now());
				long todayLong = today.getTime();
				long maxLong = Long.MAX_VALUE;
				ArrayList<String> winners = new ArrayList<String>();
				for(Entry<String, Date> entry : main.bets.entrySet()) {
					long entryLong = entry.getValue().getTime();
					long diff = Math.abs(entryLong-todayLong);
					if(diff<maxLong) {
						winners = new ArrayList<String>();
						 winners.add(entry.getKey());
						maxLong = diff;
					}else if(diff==maxLong) {
						winners.add(entry.getKey());
					}
				}
				for(String s : winners) {
					event.getGuild().getTextChannelById(Variable.getGeneral()).sendMessage("F�licitation "+main.jda.getUserById(s).getAsMention()+" tu as gagn� cette manche :wink:").queue();
				}
							
				
				
				long actualTime = OffsetDateTime.now().toEpochSecond();
				if (main.theodoreConnection == 0) {
					event.getGuild().getTextChannelById(Variable.getGeneral()).sendMessage("OH MON DIEU "
							+ event.getUser().getAsMention() + ", NOTRE SAUVEUR VIENT DE SE CONNECTER\r\n").queue();
				} else {
					if (actualTime >= main.theodoreConnection + 604800) {
						String time = "";
						long timeLong = actualTime - main.theodoreConnection;
						time = (timeLong % 60) + " secondes" + time;
						time = ((timeLong / 60) % 60) + " minutes et " + time;
						time = ((timeLong / 3600) % 24) + " heures, " + time;
						time = (timeLong / 86400) + " jours, " + time;
						event.getGuild().getTextChannelById(Variable.getGeneral())
								.sendMessage("OH MON DIEU " + event.getUser().getAsMention()
										+ ", NOTRE SAUVEUR VIENT DE SE CONNECTER\r\n"
										+ "Il ne s'�tait pas connect� depuis " + time)
								.queue();
					}
				}
			} else {
				main.theodoreConnection = OffsetDateTime.now().toEpochSecond();
			}
		}
	}
}
