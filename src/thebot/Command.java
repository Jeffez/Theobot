package thebot;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public interface Command {

	public boolean called(String[] var1, MessageReceivedEvent var2);

	////////////////////////////////////////////

	public void action(String[] var1, MessageReceivedEvent var2);

	////////////////////////////////////////////

	public String help();

	////////////////////////////////////////////

	public void executed(boolean var1, MessageReceivedEvent var2);
}
